package com.hitang.smarttourism;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;


public class AugmentRecyclerAdapter extends RecyclerView.Adapter<AugmentRecyclerAdapter.CustomViewHolder> {
    private List<Augment> augmentList;
    private Context mContext;
    private View thisview;

    public AugmentRecyclerAdapter(Context context, List<Augment> augmentList) {
        this.augmentList = augmentList;
        this.mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_augment, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, int i) {
        final Augment augment = augmentList.get(i);
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomViewHolder holder = (CustomViewHolder) view.getTag();
                int position = holder.getLayoutPosition();
//                Augment school = augmentList.get(position);
                Toast.makeText(mContext, position, Toast.LENGTH_SHORT).show();
            }
        };

//Handle click event on both title and image click

        //Download image using picasso library
        if (augment.getImage().isEmpty()) {
            customViewHolder.imageView.setImageResource(R.drawable.ic_offline);
        } else {
            Picasso.with(mContext).load(AppFields.URL_IMAGE_FOLDER + augment.getImage())
                    .error(R.drawable.ic_offline)
                    .placeholder(R.drawable.ic_offline)
                    .into(customViewHolder.imageView);
        }
//        MaterialRippleLayout.on(customViewHolder.cardView)
//                .rippleColor(Color.parseColor("#FF0000"))
//                .rippleAlpha(0.2f)
//                .rippleHover(true)
//                .create();
        customViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = customViewHolder.getAdapterPosition();
            }
        });
        customViewHolder.title.setText(augment.getTitle());
//        customViewHolder.title.setText(augment.getTitle());
//        customViewHolder.title.setVisibility(View.GONE);
        customViewHolder.message.setText( Html.fromHtml(augment.getBody()));

    }

    @Override
    public int getItemCount() {
        return (null != augmentList ? augmentList.size() : 0);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView title, message;
        protected CardView cardView;
        ImageView imageView;


        public CustomViewHolder(View view) {
            super(view);
            this.title = (TextView) view.findViewById(R.id.augment_title);
            this.message = (TextView) view.findViewById(R.id.augment_message);
            this.cardView = (CardView) view.findViewById(R.id.augment_card);
            this.imageView = (ImageView) view.findViewById(R.id.imageView);

        }
    }
}