package com.hitang.smarttourism;

/**
 * Created by Code on 28/04/2017.
 */

public class AppFields {
    public static int EMPTY_LIST = 1;
    public static int OFFLINE = 2;
    public static int ERROR = 3;
    public static String URL_TRENDING_AUGMENTS = "http://192.168.43.249/project/openaug/public/api/posts/all";
    public static String URL_AUGMENTS = "http://192.168.43.249/project/openaug/public/api/posts/";
    public static String URL_IMAGE_FOLDER = "http://192.168.43.249/project/openaug/public/images/";
}
