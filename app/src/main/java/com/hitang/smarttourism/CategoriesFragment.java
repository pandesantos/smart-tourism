package com.hitang.smarttourism;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;

import static android.provider.Contacts.PresenceColumns.OFFLINE;
import static com.hitang.smarttourism.AppFields.EMPTY_LIST;
import static com.hitang.smarttourism.AppFields.ERROR;


public class CategoriesFragment extends Fragment {
    ArrayList<SimpleKV> categoryList = new ArrayList<SimpleKV>();
    private CategoryRecyclerAdapter adapter;
    String userID;
    RequestQueue queue;
    RelativeLayout no_category_rl;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    Context context;
    TextView error_title_tv, error_message_tv;
    ImageView error_iv;
    String offline_title, offline_message;
    String error_messsage, error_title;
    String empty_messsage, empty_title;

    public CategoriesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_categories, container, false);
        no_category_rl = (RelativeLayout) rootView.findViewById(R.id.no_assignments_rl);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        error_message_tv = (TextView) rootView.findViewById(R.id.error_message);
        error_title_tv = (TextView) rootView.findViewById(R.id.error_title);
        error_iv = (ImageView) rootView.findViewById(R.id.error_image);
        context = getContext();
        queue = Volley.newRequestQueue(context);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.category_list);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        adapter = new CategoryRecyclerAdapter(context, categoryList);
        recyclerView.setAdapter(adapter);
        LoadNotifications();
        return rootView;
    }

    public void LoadNotifications() {
        categoryList.add(new SimpleKV("software", "Software"));
        categoryList.add(new SimpleKV("hotel", "Hotels"));
        categoryList.add(new SimpleKV("restaurants", "Restaurants"));
        categoryList.add(new SimpleKV("inn", "Inn"));
        categoryList.add(new SimpleKV("lodges", "Lodges"));
        categoryList.add(new SimpleKV("pub", "Pub"));
        categoryList.add(new SimpleKV("shopping", "Shopping"));
        adapter.notifyDataSetChanged();
        checkIfNoNotifications();
    }

    private void error(boolean error, int type) {
//        swipeRefreshLayout.setRefreshing(false);
        if (error) {
            recyclerView.setVisibility(View.GONE);
            if (type == EMPTY_LIST) {
                error_title_tv.setText(empty_title);
                error_message_tv.setText(empty_messsage);
                error_iv.setImageResource(R.drawable.ic_sentiment_dissatisfied);
            } else if (type == OFFLINE) {
                error_title_tv.setText(offline_title);
                error_message_tv.setText(offline_message);
                error_iv.setImageResource(R.drawable.ic_offline);
            } else if (type == ERROR) {
                error_title_tv.setText(error_title);
                error_message_tv.setText(error_messsage);
                error_iv.setImageResource(R.drawable.ic_sentiment_dissatisfied);
            }
        } else {
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    public void checkIfNoNotifications() {
        if (categoryList.size() == 0) {
            error(true, EMPTY_LIST);
        } else {
            error(false, 0);
        }
    }
}
