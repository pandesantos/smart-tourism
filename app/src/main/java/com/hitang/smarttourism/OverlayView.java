package com.hitang.smarttourism;

import android.content.Context;
import android.graphics.Canvas;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Code on 27/04/2017.
 */

public class OverlayView extends View implements SensorEventListener, LocationListener {
    float[] gyroValues, comValues, accValues;
    LocationManager locMgr;
      public OverlayView(Context context) {
        super(context);
        SensorManager sensors = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        Sensor accelSensor = sensors.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        Sensor compassSensor = sensors.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        Sensor gyroSensor = sensors.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        boolean isAccelAvailable = sensors.registerListener(this, accelSensor, SensorManager.SENSOR_DELAY_NORMAL);
        boolean isCompassAvailable = sensors.registerListener(this, compassSensor, SensorManager.SENSOR_DELAY_NORMAL);
        boolean isGyroAvailable = sensors.registerListener(this, gyroSensor, SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    protected void onDraw(Canvas canvas) {
//        try {
//            Thread.sleep(100);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        super.onDraw(canvas);
        float w, h, cw, ch;
        w = canvas.getWidth();
        h = canvas.getHeight();
        cw = w / 2;
        ch = h / 2;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        StringBuilder msg = new StringBuilder(event.sensor.getName()).append(" ");
        for (float value : event.values) {
            msg.append("[").append(value).append("]");
        }

        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
//                accelData = msg.toString();
                accValues = event.values;
                break;
            case Sensor.TYPE_GYROSCOPE:
                for (float value : event.values) {
                    msg.append("[").append(value * 100).append("]");
                }
                gyroValues = event.values;
//                gyroData = msg.toString();
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                comValues = event.values;
//                compassData = msg.toString();
                break;
        }

        this.invalidate();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
