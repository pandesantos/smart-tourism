package com.hitang.smarttourism;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;


public class CategoryRecyclerAdapter extends RecyclerView.Adapter<CategoryRecyclerAdapter.CustomViewHolder> {
    private List<SimpleKV> categoryList;
    private Context mContext;
    private View thisview;

    public CategoryRecyclerAdapter(Context context, List<SimpleKV> categoryList) {
        this.categoryList = categoryList;
        this.mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_category, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, int i) {
        final SimpleKV category = categoryList.get(i);
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomViewHolder holder = (CustomViewHolder) view.getTag();
                int position = holder.getLayoutPosition();
//                Augment school = categoryList.get(position);
                Toast.makeText(mContext, position, Toast.LENGTH_SHORT).show();
            }
        };
        customViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment;
                fragment = new AugmentsFragment();
//                fragment = new TestFrag();
                FragmentManager fragmentManager = ((FragmentActivity) mContext).getSupportFragmentManager();
                fragmentManager.beginTransaction().addToBackStack("to_note_details")
                        .replace(R.id.augments_fragment, fragment, "TAG_NOTE_DETAILS")
                        .commit();
            }
        });
//Handle click event on both title and image click

        //Download image using picasso library
//        if (category.getImage().isEmpty()) {
//            customViewHolder.imageView.setImageResource(R.drawable.ic_offline);
//        } else {
//            Picasso.with(mContext).load(AppFields.URL_IMAGE_FOLDER + category.getImage())
//                    .error(R.drawable.ic_offline)
//                    .placeholder(R.drawable.ic_offline)
//                    .into(customViewHolder.imageView);
//        }
//        MaterialRippleLayout.on(customViewHolder.cardView)
//                .rippleColor(Color.parseColor("#FF0000"))
//                .rippleAlpha(0.2f)
//                .rippleHover(true)
//                .create();
        customViewHolder.title.setText(category.getValue());
        customViewHolder.cardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.button_orange_normal));
//        customViewHolder.title.setText(category.getTitle());
//        customViewHolder.title.setVisibility(View.GONE);
//        customViewHolder.message.setText( Html.fromHtml(category.getBody()));

    }

    @Override
    public int getItemCount() {
        return (null != categoryList ? categoryList.size() : 0);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView title, message;
        protected CardView cardView;
        ImageView imageView;


        public CustomViewHolder(View view) {
            super(view);
            this.title = (TextView) view.findViewById(R.id.category_title);
            this.cardView = (CardView) view.findViewById(R.id.category_card);
            this.imageView = (ImageView) view.findViewById(R.id.imageView);

        }
    }
}