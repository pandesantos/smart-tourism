package com.hitang.smarttourism;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.github.clans.fab.FloatingActionMenu;

/**
 * Created by Code on 04/11/2016.
 */

public class PagerAdapter extends FragmentPagerAdapter {
    int mNumOfTabs;
    FloatingActionMenu fam;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
//        this.fam=fam;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TrendingFragment tab1 = new TrendingFragment();
                return tab1;
            case 1:
                CategoriesFragment tab2 = new CategoriesFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}