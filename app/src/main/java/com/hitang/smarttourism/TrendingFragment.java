package com.hitang.smarttourism;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import im.delight.android.location.SimpleLocation;

import static android.provider.Contacts.PresenceColumns.OFFLINE;
import static com.hitang.smarttourism.AppFields.EMPTY_LIST;
import static com.hitang.smarttourism.AppFields.ERROR;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TrendingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TrendingFragment extends Fragment {
    ArrayList<Augment> augmentList = new ArrayList<Augment>();
    private AugmentRecyclerAdapter adapter;
    String userID;
    RequestQueue queue;
    RelativeLayout no_augment_rl;
    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    Context context;
    TextView error_title_tv, error_message_tv;
    ImageView error_iv;
    String offline_title, offline_message;
    String error_messsage, error_title;
    String empty_messsage, empty_title;
    private SimpleLocation location;

    public TrendingFragment() {
        // Required empty public constructor
    }


    public static TrendingFragment newInstance(String param1, String param2) {
        TrendingFragment fragment = new TrendingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_trending, container, false);
        no_augment_rl = (RelativeLayout) rootView.findViewById(R.id.no_assignments_rl);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.augment_swipe_refresh_layout);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        error_message_tv = (TextView) rootView.findViewById(R.id.error_message);
        error_title_tv = (TextView) rootView.findViewById(R.id.error_title);
        error_iv = (ImageView) rootView.findViewById(R.id.error_image);
        context = getContext();
        queue = Volley.newRequestQueue(context);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.augment_list);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
        adapter = new AugmentRecyclerAdapter(context, augmentList);
        recyclerView.setAdapter(adapter);
        LoadNotifications();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                error(false, 0);
                swipeRefreshLayout.setRefreshing(false);
                LoadNotifications();
            }
        });
        return rootView;
    }

    public void LoadNotifications() {
//        if (InternetConnectionCheck.isInternetAvailable(context)) {
            progressBar.setVisibility(View.VISIBLE);
            String url = AppFields.URL_TRENDING_AUGMENTS;
            StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressBar.setVisibility(View.GONE);
                            try {
                                error(false, 0);
                                System.out.println("augment response" + response);
//                                new AlertDialog.Builder(NotificationActivity.this).setMessage(response).show();
                                augmentList.clear();
                                JSONArray jsonarray = new JSONArray(response);
                                for (int i = 0; i < jsonarray.length(); i++) {
                                    JSONObject jsonobject = jsonarray.getJSONObject(i);
                                    Augment augment = new Augment();
                                    augment.setTitle(jsonobject.getString("title"));
                                    augment.setBody(jsonobject.getString("body"));
                                    augment.setImage(jsonobject.getString("image"));
                                    augment.setSlug(jsonobject.getString("slug"));
                                    augment.setId(jsonobject.getInt("id"));
                                    augment.setLatitude(jsonobject.getDouble("lat"));
                                    augment.setLongitude(jsonobject.getDouble("long"));
                                    JSONObject categoryObject = new JSONObject(jsonobject.getString("category"));
                                    augment.setCategory_name(categoryObject.getString("name"));
                                    augment.setCreated_at(jsonobject.getString("created_at"));
                                    augment.setEdited_at(jsonobject.getString("updated_at"));
                                    augmentList.add(augment);
                                    augmentList.add(augment);
                                    augmentList.add(augment);
                                    augmentList.add(augment);
                                    augmentList.add(augment);
                                    adapter.notifyDataSetChanged();
                                }
                                checkIfNoNotifications();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressBar.setVisibility(View.GONE);
                            error(true, ERROR);
                        }
                    }
            );
            queue.add(postRequest);
//        } else {
//            error(true, OFFLINE);
//        }
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void error(boolean error, int type) {
        swipeRefreshLayout.setRefreshing(false);
        if (error) {
            recyclerView.setVisibility(View.GONE);
            if (type == EMPTY_LIST) {
                error_title_tv.setText(empty_title);
                error_message_tv.setText(empty_messsage);
                error_iv.setImageResource(R.drawable.ic_sentiment_dissatisfied);
            } else if (type == OFFLINE) {
                error_title_tv.setText(offline_title);
                error_message_tv.setText(offline_message);
                error_iv.setImageResource(R.drawable.ic_offline);
            } else if (type == ERROR) {
                error_title_tv.setText(error_title);
                error_message_tv.setText(error_messsage);
                error_iv.setImageResource(R.drawable.ic_sentiment_dissatisfied);
            }
        } else {
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    public void checkIfNoNotifications() {
        if (augmentList.size() == 0) {
            error(true, EMPTY_LIST);
        } else {
            error(false, 0);
        }
    }
}
